import React from 'react';
import Profile, { ProfileUser } from '../../components/profile';

interface ProfilesListProps {
}

interface ProfilesListViewState {
}

class ProfilesListView extends React.Component {

  render() {
    return (
      <div className="container container--margin">
        <div className="content__header">
          <h1>Liste des profiles disponibles</h1>
        </div>
        <div className="content__wrapper">
          {this.mockUsers().map((pr: ProfileUser) => {
            return <Profile profile={pr} />
          })}
        </div>
      </div>
    )
  }



  mockUsers(): ProfileUser[] {
    return [
      {
        name: "Bryant",
        firstname: "Kobe",
        description: "joeur des lakers",
        skills: ["AR", "shoot", "dunk"],
        age: 41
      }, {
        name: "Jordan",
        firstname: "Michael",
        description: "joeur des Bull",
        skills: ["AR", "shoot", "dunk", "meneur"],
        age: 56
      }]
  }
}

export default ProfilesListView;