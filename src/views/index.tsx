import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';

import { Switch, Route } from 'react-router-dom';

import routes from '../routes';

import HomeView from './home';
import AboutView from './about';
import ProfilesListView from './profiles-list';


const RootView: React.StatelessComponent<{}> = () => (
  <Fragment>
    <Helmet
      titleTemplate='React TypeScript Boilerplate - %s'
      defaultTitle='React TypeScript Boilerplate'
    />

    <main>
      <Switch>
        <Route path={routes.HOME} component={HomeView}/>
        <Route path={routes.PROFILES} component={ProfilesListView}/>
        <Route path={routes.ABOUT} component={AboutView}/>
      </Switch>
    </main>
  </Fragment>
);

export default RootView;