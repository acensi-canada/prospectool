import React from 'react';
import request from 'superagent';

interface AboutProps {
}

interface AboutState {
  loaded: boolean;
  profil?: any;
}

class About extends React.Component<AboutProps, AboutState> {
  constructor(props: AboutProps) {
    super(props);
    this.state = {
      loaded: false
    }
  }


  componentDidMount() {
    request
      .get(`${process.env.REACT_APP_API}/profiles`)
      .withCredentials()
      .end((err, res) => {
        if (err) {
          console.log('error :', err);
        } else {
          const data = JSON.parse(res.text);
          this.setState({
            loaded: true,
            profil: data
          })
        }
      });
  }

  render() {
    return (
      this.state.loaded &&
      <div className="container container--margin">
        <div className="content__header">
          <h1>A propos</h1>
        </div>
        <div className="content__wrapper">
          Retour des API :
          {this.state.profil.map((elem: any) => {
            return <p>{elem.firstname} - {elem.lastname}</p>
          })}
        </div>
      </div>
    )
  }
}

export default About;