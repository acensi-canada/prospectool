import React from 'react';

interface HomeViewProps {
}

interface HomeViewState {
}

class HomeView extends React.Component {

  render() {
    return(
      <div className="container container--margin">
        <div className="content__header">
            <h1>Page d'accueil</h1>
        </div>
      <div className="content__wrapper">
        La on met des trucs cools
      </div>
    </div>
    )
  }
} 

export default HomeView;