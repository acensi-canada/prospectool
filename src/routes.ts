/**
 * For nested routes use:
 * import withParentPrefix from './utils/with-parent-prefix';
 *
 * Example:
 * {
 *   dashboard: withParentPrefix('/dashboard', {
 *     ACCOUNT: '/account'
 *   }
 * }
 */

const routes = {
  HOME: '/home',
  PROFILES: "/profiles-list",
  ABOUT: '/about'
};

export default routes;
