import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux'
import todoApp from './reducers'
import Root from './views';

import history from './utils/history';
import { ROOT_NODE } from './constants';
import Header from './components/header';
import './styles/main.scss';
import Menu from './components/menu';


const store = createStore(todoApp)

const render = () => {
  ReactDOM.render(
    (
      <Provider store={store}>
        <Router history={history}>
          <Menu />
          <Header />
          <Root/>
        </Router>
      </Provider>
    ),
    ROOT_NODE as HTMLElement,
  );
};

if ((module as any).hot) {
  (module as any).hot.accept(() => {
    ReactDOM.unmountComponentAtNode(ROOT_NODE as HTMLElement);
    render();
  });
}

render();
