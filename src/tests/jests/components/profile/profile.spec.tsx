import React from 'react';
import TestRenderer  from 'react-test-renderer';
import Profile from '../../../../components/profile';

const profile = 
  {
  name : "Bryant",
  firstname : "Kobe",
  description : "joeur des lakers",
  skills : ["AR", "shoot", "dunk"],
  age : 41
}

describe('Profile', () => {
  it('snapshot renders', () => {
    expect(2 + 2).toEqual(4);
    const component = TestRenderer.create(<Profile profile={profile}/>);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});