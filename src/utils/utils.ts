
export default class Utils {

    private static configFile = require('./../config/config.json');
    private static defaultFile = require('./../config/default.json')
    private static variables: any = Utils.configFile.env === "$ENV" ? Utils.defaultFile : Utils.configFile;
    

    static get(variable : string) {
        if (Utils.variables[variable]) {
            return Utils.variables[variable];
        } else {
            return null;
        }
    }

}