import React from 'react';
import { Link } from 'react-router-dom';

export interface NavigationElem {
    value : string,
    label : string
}
export interface NavigationProps {
    navigation: NavigationElem[]
}

export interface NavigationState {
}

export default class Navigation extends React.Component<NavigationProps, NavigationState> {

    render() {
        return (
            <div>
                <ul className="menu__liste">
                    {this.props.navigation.map((nav : NavigationElem )=> {
                        return (<li><Link to={nav.value}>{nav.label}</Link></li>)
                    })}
                </ul>
            </div>
        );
    }
}