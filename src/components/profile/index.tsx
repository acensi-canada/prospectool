import React from 'react';

export interface ProfileUser {
    name ?: string,
    firstname? : string,
    description: string,
    age ?: number,
    skills : string[]
}
export interface ProfileProps {
   profile: ProfileUser
}

export interface ProfileState {
}


export default class Profile extends React.Component<ProfileProps, ProfileState> {

    render() {
        return (
            <div>
                {this.props.profile.name && <p>{this.props.profile.name}</p>}
                {this.props.profile.firstname && <p>{this.props.profile.firstname}</p>}
                {this.props.profile.age && <p>{this.props.profile.age}</p>}
                {<p>{this.props.profile.description}</p>}
                {<p>{this.props.profile.skills}</p>}
                --------------
            </div>
        );
    }
}