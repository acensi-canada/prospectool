import React from 'react';

export interface HeaderProps {
}

export interface HeaderState {
}



export default class Header extends React.Component<HeaderProps, HeaderState> {

    render() {
        return (
            <div className="grid && grid--row && header">
                <h1 className="header__title">Projet de visualisation de profils</h1>
            </div>
        );
    }
}