import React from 'react';
import Navigation, { NavigationElem } from '../navigation';

const navigation:NavigationElem[] = [{value : "home", label : "Home"}, {value: "profiles-list", label : "Profils"}, {value : "about", label : "About"}];


export default class Menu extends React.Component<any, any> {

    render() {
        return (
            <div className="menu">
                <img src="/react-logo.svg" alt="logo react bleu" width="80%"/>
                <Navigation navigation = {navigation}></Navigation>
            </div>
        );
    }
}